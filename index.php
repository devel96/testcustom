<?php

// global functions

function d($arg)
{
    echo '<pre>';
        print_r($arg);
    echo '</pre>';
    die;
}

// Define constants
const TASK1 = 1;
const TASK2 = 2;
const TASK3 = 3;

$taskNumber = $_GET['task'] ?? TASK1;

if (!in_array($taskNumber, [TASK1, TASK2, TASK3])) {
    throw new Exception('Wrong task number');
}

require_once "task" . $taskNumber . "/index.php";