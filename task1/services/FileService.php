<?php

namespace app\services;

require_once ROOT . "/models/FileObject.php";
require_once ROOT . "/models/StringObject.php";

use app\models\FileObject;
use app\models\StringObject;

class FileService
{
    const DEFAULT_UPLOAD_FILE_KEY = 'uploadedFile';

    /**
     * @param string $name
     * @return FileObject|null
     */
    public static function getUploadedFile(string $name = self::DEFAULT_UPLOAD_FILE_KEY): ?FileObject
    {
        if (isset($_FILES[$name])) {
            return new FileObject($_FILES[$name]);
        }

        return null;
    }

    /**
     * @param FileObject $file
     */
    public static function saveFile(FileObject $file)
    {
        $dir = ROOT . '/uploads';
        if (!is_dir($dir)) {
            mkdir($dir);
        }

        move_uploaded_file($file->getTmpName(), $dir . '/' . $file->getName());
    }

    /**
     * @param string $name
     * @param string $delimeter
     * @return array
     */
    public static function getFileData(string $name, string $delimeter = ' '): array
    {
        // Если файл будет большим надо построчно читать
        $arr = explode($delimeter, file_get_contents(ROOT . '/uploads/' . $name) ?? '');
        $stringObjects = [];
        foreach ($arr as $item) {
            $stringObjects[] = new StringObject($item, preg_match_all("/[0-9]/", $item));
        }

        return $stringObjects;
    }
}