<?php

/**
 * @var $uploadedFile FileObject;
 * @var $validationError string;
 * @var $data array;
 */

use app\models\FileObject;
use app\models\StringObject;

?>

<!DOCTYPE html>
<html>
<head>
    <style>
        .red {
            border: 1px solid red;
        }

        .green {
            border: 1px solid green;
        }

        form {
            width: 400px;
            padding: 30px;
        }
    </style>
</head>
<body>
<form action="/" method="post" enctype="multipart/form-data"
      class="<?= $validationError ? 'red' : ($uploadedFile ? 'green' : '') ?>">
    Select image to upload:
    <input type="file" name="uploadedFile" id="fileToUpload">
    <input type="submit" value="Upload" name="submit">
</form>

<?php if ($uploadedFile && !$validationError) : ?>
    <ul>
        <?php foreach ($data as $stringObject) : ?>
            <?php /** @var StringObject $stringObject */ ?>
            <li><?= $stringObject->getText() . ' : ' . $stringObject->getCount() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

</body>
</html>
