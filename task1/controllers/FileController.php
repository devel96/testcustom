<?php


namespace app\controllers;

require_once ROOT . "/services/FileService.php";

use app\services\FileService;

/**
 * Class FileController
 * @package app\controllers
 */
class FileController
{
    /**
     * Action run
     */
    public function run()
    {
        $uploadedFile = FileService::getUploadedFile();
        $validationError = $uploadedFile ? $uploadedFile->getValidationError() : '';
        $data = '';

        if (!$validationError && $uploadedFile) {
            FileService::saveFile($uploadedFile);
            $data = FileService::getFileData($uploadedFile->getName());
        }


        require_once ROOT . "/views/index.php";
    }
}