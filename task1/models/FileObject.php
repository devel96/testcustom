<?php


namespace app\models;

/**
 * Class FileObject
 * @package app\models
 */
class FileObject
{
    private string $name;
    private string $type;
    private string $tmpName;
    private string $error;
    private string $size;

    /**
     * FileObject constructor.
     * @param array $file
     */
    public function __construct(array $file)
    {
        $this->name = $file['name'] ?? '';
        $this->type = $file['type'] ?? '';
        $this->tmpName = $file['tmp_name'] ?? '';
        $this->error = $file['error'] ?? '';
        $this->size = $file['size'] ?? '';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTmpName(): string
    {
        return $this->tmpName;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @return string|null
     */
    public function getValidationError(): ?string
    {
        return $this->type !== 'text/plain' ? 'Please upload only txt format' : '';
    }
}