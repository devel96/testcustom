<?php


namespace app\models;

/**
 * Class StringObject
 * @package app\models
 */
class StringObject
{
    private string $text;
    private int $count;

    public function __construct(string $text, int $count)
    {
        $this->text = $text;
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}